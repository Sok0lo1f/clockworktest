import React, { FC, useState } from 'react'
import { Link } from 'react-router-dom'
import { Icon } from 'semantic-ui-react'
import './Header.scss'

interface IHeaderProps {
    setOpen: (props: boolean) => void
}

export const Header: FC<IHeaderProps> = ({ setOpen }) => {
    const [isDropdownOpen, setDropdownOpen] = useState(false)
    return (
        <header className='header'>
            <div className='header__menu-open-mobile'>
                <Icon onClick={() => setOpen(true)} name='bars' />
            </div>
            <Link to='/' className='header__logo'>
                <Icon name='apple' />
                Developer
            </Link>
            <div className='header__name'>Account</div>
            <section
                className='header__user-block'
                onClick={() => setDropdownOpen(!isDropdownOpen)}>
                <div className='header__user-block--info'>
                    <div>
                        <span>Alexander Sokolov</span>
                        <span>KLOKVORK, OOO</span>
                    </div>
                    <Icon name='angle down' />
                </div>
                <ul
                    className={`header__user-block--dropdown ${
                        isDropdownOpen ? 'open' : ''
                    }`}>
                    <li className='header__user-block--dropdown-element'>
                        There is
                    </li>
                    <li className='header__user-block--dropdown-element'>
                        Something
                    </li>
                    <li className='header__user-block--dropdown-element'>
                        Info
                    </li>
                    <li className='header__user-block--dropdown-element'>
                        Logout
                    </li>
                </ul>
            </section>
        </header>
    )
}
