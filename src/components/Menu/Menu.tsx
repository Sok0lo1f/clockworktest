import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { Icon } from 'semantic-ui-react'
import { SemanticICONS } from 'semantic-ui-react/dist/commonjs/generic'
import './Menu.scss'

interface IResources {
    icon: SemanticICONS
    linkName: string
    link: string
}
interface IMenuProps {
    isOpen: boolean
    setOpen: (props: boolean) => void
}
const progResources: IResources[] = [
    {
        icon: 'list',
        linkName: 'Overview',
        link: '/overview',
    },
    {
        icon: 'info circle',
        linkName: 'Membership',
        link: '/membership',
    },
    {
        icon: 'user circle outline',
        linkName: 'People',
        link: '/people',
    },
    {
        icon: 'certificate',
        linkName: 'Certificates, IDs & Profiles',
        link: '/profiles',
    },
    {
        icon: 'app store',
        linkName: 'App Store Connect',
        link: '/store',
    },
    {
        icon: 'cloudversify',
        linkName: 'CloudKit Console',
        link: '/cloudkit',
    },
    {
        icon: 'wrench',
        linkName: 'Code-Level Support',
        link: '/support',
    },
]
const addResources: IResources[] = [
    {
        icon: 'file text',
        linkName: 'Documentation',
        link: '/docs',
    },
    {
        icon: 'download',
        linkName: 'Downloads',
        link: '/downloads',
    },
    {
        icon: 'chat',
        linkName: 'Forums',
        link: '/forums',
    },
    {
        icon: 'envelope outline',
        linkName: 'Feedback Assistant',
        link: '/feedback',
    },
    {
        icon: 'question circle outline',
        linkName: 'Account Help',
        link: '/help',
    },
    {
        icon: 'phone volume',
        linkName: 'Contact Us',
        link: '/contacts',
    },
]

const MenuElement: FC<IResources> = (props) => {
    const { linkName, icon, link } = props
    return (
        <li className='menu-element'>
            <Link className='menu-element-link' to={link}>
                <Icon name={icon} />
                <div>{linkName}</div>
            </Link>
        </li>
    )
}

export const Menu: FC<IMenuProps> = ({ isOpen, setOpen }) => {
    const menuHandler = (e: React.MouseEvent) => {
        if (e.target === e.currentTarget) setOpen(false)
    }
    return (
        <section
            className={`menu-mobile-wrapper ${isOpen ? 'open' : ''}`}
            onClick={menuHandler}>
            <ul className='menu'>
                <li className='menu__mobile'>
                    <Link to='/' className='menu__mobile-logo'>
                        <Icon name='apple' />
                        Developer
                    </Link>
                    <Icon onClick={() => setOpen(false)} name='close' />
                </li>

                <li className='menu__header'>
                    <span>Program Resources</span>
                </li>
                {progResources.map((resource, index) => (
                    <MenuElement
                        key={`${index}_prog`}
                        icon={resource.icon}
                        linkName={resource.linkName}
                        link={resource.link}
                    />
                ))}
                <li className='menu__header'>
                    <span>Additional Resources</span>
                </li>
                {addResources.map((resource, index) => (
                    <MenuElement
                        key={`${index}_add`}
                        icon={resource.icon}
                        linkName={resource.linkName}
                        link={resource.link}
                    />
                ))}
            </ul>
        </section>
    )
}
