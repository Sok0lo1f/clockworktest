import React, { FC } from 'react'
import { Icon } from 'semantic-ui-react'
import { SemanticICONS } from 'semantic-ui-react/dist/commonjs/generic'
import './Card.scss'

interface ICardProps {
    image: SemanticICONS
    name: string
    description: string
}

export const Card: FC<ICardProps> = ({ image, name, description }) => {
    return (
        <div className='card'>
            <Icon className='card__icon' name={image} />
            <span className='card__name'>{name}</span>
            <span className='card__description'>{description}</span>
        </div>
    )
}
