import React from 'react'
import './MainPage.scss'
import Cards from '../../components/Card/Card.json'
import { Card } from '../../components/Card/Card'
import { SemanticICONS } from 'semantic-ui-react'

export const MainPage = () => {
    return (
        <section className='main-page'>
            <div className='main-page__header'>
                <p className='main-page__header-name'>KLOKVORK, OOO</p>
                <p className='main-page__header-description'>
                    Apple Developer Program
                </p>
            </div>
            <div className='main-page__cards'>
                {Cards.map((card, index) => (
                    <Card
                        key={`${index}__card`}
                        image={card.image as SemanticICONS}
                        name={card.name}
                        description={card.description}
                    />
                ))}
            </div>
        </section>
    )
}
