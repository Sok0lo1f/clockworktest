import React, { useState } from 'react'
import { Route, Routes } from 'react-router'
import './App.css'
import { Header } from './components/Header/Header'
import { Menu } from './components/Menu/Menu'
import { MainPage } from './pages/MainPage/MainPage'

const App = () => {
    const [isOpen, setOpen] = useState(false)
    return (
        <>
            <Header setOpen={setOpen} />
            <Menu isOpen={isOpen} setOpen={setOpen} />
            <Routes>
                <Route path='/' element={<MainPage />} />
                <Route path='*' element={<div>404</div>} />
            </Routes>
        </>
    )
}

export default App
